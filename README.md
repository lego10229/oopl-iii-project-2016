# Documentation #
 
### Author: ###
  
 Klaudia Brzezińska
 
### Description ###
  
 Database Instruments (written in Java language).

My project is about music school/istruments. It works like a database and I can display a special instrument (like doublebass with 5 strings). and instrument which I have in my music school.

### How to compile ###

If you want to compile my project, just use Maven to build this app.

### How to run ###

Run:

    java -cp project-java-0.0.1-SNAPSHOT.jar MainClass
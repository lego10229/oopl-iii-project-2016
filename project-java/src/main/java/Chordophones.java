public class Chordophones extends Instrument implements InterfacePlayChordo{
	
	public String stringAmount;

	public Chordophones(){};
	public Chordophones(String name, String stringAmount){
		this.name=name;
		this.stringAmount= stringAmount;
	}
	
	public void plucking() {
		System.out.println("Instrument are plucking");
		
	}
	public void strumming() {
		System.out.println("Instrument are strumming");
		
	}
	public void rubbingWithABow() {
		System.out.println("Instrument are rubbingWithABow");
		
	}
	public void striking() {
		System.out.println("Instrument are striking");
		
	}

}

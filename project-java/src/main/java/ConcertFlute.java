public class ConcertFlute extends Aerophones implements InterfacePlayAero  {
	public ConcertFlute(){};
	public ConcertFlute(String name,String highSound, String lowSound) {
		this.name=name;
		this.highSound = highSound;
		this.lowSound = lowSound;
	}
	@Override
	public String toString() {
		return "ConcertFlute [HighSound=" + highSound + ", LowSound=" + lowSound + ", Polish name=" + name + "]";
	}
	public void play (){
		System.out.println("Concertflute are playing");
		
	}
	public void clean()
	{
		
			System.out.println("Concertflute - bloked!");
			try{
				Thread.sleep(3000);
				}catch (Exception e) {
					   System.out.println(e);
				   }
			
		System.out.println("Cleaning.. ready!");
	}
}

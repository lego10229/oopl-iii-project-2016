public class Doublebass extends Chordophones implements InterfacePlayChordo {
	
	public Doublebass(){};
	public Doublebass(String name, String stringAmount){
		this.name=name;
		this.stringAmount= stringAmount;
	}
	@Override
	public String toString() {
		return "Doublebass [StringAmount=" + stringAmount + ", name=" + name + "]";
	}
	@Override
	public void plucking(){
		System.out.println("Double bas are plucking");
	}
	@Override
	public void strumming(){
		System.out.println("Double bas are strumming");
	}
	@Override
	public void rubbingWithABow(){
		System.out.println("Double bas are playing with a bow");
	}
	@Override
	public void striking(){
		System.out.println("Double bas are striking");
	}

}

public interface InterfacePlayChordo {

	public void plucking();
	public void strumming();
	public void rubbingWithABow();
	public void striking();
}

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class MainClass {
	public static void main(String[] args) throws ExceptionInstruments
	{
		System.out.println("Welcome to the Storehouse of Music school ");
		System.out.println("You can display to list of instruments");
		System.out.println("Write 1 to display what we have, write 2 to display orchester instrument");
		
	int choice; 
	   Scanner scanner = new Scanner(System.in); 
	   choice = scanner.nextInt();
	   scanner.close();
	      ExceptionInstruments exception = new ExceptionInstruments();
	      exception.CheckNumber(choice);
	      
	  	Doublebass doublebass1 = new Doublebass("kontrabas Anii","4");
		Doublebass doublebass2 = new Doublebass("kontrabas Piortka","5");
		Doublebass doublebass3 = new Doublebass("kontrabas Zuzi","4");
		List<Instrument> lista = new LinkedList<Instrument>();
		lista.add(new Doublebass("kontrabas 1", "4"));
		lista.add(new Doublebass("kontrabas 2", "5"));
		lista.add(new ConcertFlute("flet poprzeczny 1", "C4", "B2"));
		lista.add(new ConcertFlute("flet poprzeczny 2", "D4", "C2"));
		
		Map<String, String> listaContrabas = new HashMap<String, String>();
		listaContrabas.put(doublebass1.name, doublebass1.stringAmount);
		listaContrabas.put(doublebass2.name, doublebass2.stringAmount);
		listaContrabas.put(doublebass3.name ,doublebass3.stringAmount);
		
		
	      if (choice==1)
	      {
	    	  System.out.println(lista);  
	    	  
	      }
	      if (choice ==2)
	      {
	    	  
	    	  for(Map.Entry<String, String> entry : listaContrabas.entrySet()){
	  			if (entry.getValue().equals("5"))
	  				System.out.println("Doublebass with 5 strings : " + entry.getKey());
	  		}
	      }
	      
	      try{
				Thread.sleep(5000);
				}catch (Exception e) {
					   System.out.println(e);
				   }
	      
	    System.out.println("Let's play the concert!");
		Chordophones doublebass = new Doublebass();
		doublebass.strumming();
		doublebass.rubbingWithABow();
		Aerophones concertFlute  = new ConcertFlute();
		concertFlute.play();
		concertFlute.clean();
		concertFlute.play();
	}
}
